/* eslint-disable import/no-dynamic-require */
import React from "react"
import styled from "styled-components"
import { Flex, Text } from "@engaging-tech/components"

const logoUrl = require(`${process.env.RAZZLE_PUBLIC_DIR}/img/placeholder.jpg`)

const LogoWrapper = styled(Flex)`
  user-select: none;
`

const Logo = styled.img`
  width: 36px;
  height: 36px;
`

const SiteLogo = () => (
  <LogoWrapper alignItems="center" width="auto" ml={2}>
    <Text fontSize={7} fontFamily="serif" color="dark.2" fontWeight="500">
      Parico2
    </Text>
  </LogoWrapper>
)

export default SiteLogo
