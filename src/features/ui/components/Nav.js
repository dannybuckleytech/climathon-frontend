import React from "react"
import { Navbar } from "@engaging-tech/navigation"
import { useRoutes, usePaths } from "@engaging-tech/routing"

import SiteLogo from "./SiteLogo"

const Nav = () => {
  let routes = useRoutes()
  const paths = usePaths()
  routes = []
  const navRoutes = routes.filter(route => route.path === paths.home)

  const allAccountRoutes = routes.filter(route =>
    route.path.includes(paths.account)
  )

  const userAccountRoutes = allAccountRoutes.filter(route => {
    return route.path !== paths.home
  })

  const auxillaryRoutes = allAccountRoutes.filter(
    route => route.path === paths.home
  )

  return (
    <Navbar
      sitelogo={SiteLogo}
      routes={navRoutes}
      accountRoutes={userAccountRoutes}
      auxillaryRoutes={auxillaryRoutes}
    />
  )
}

export default Nav
