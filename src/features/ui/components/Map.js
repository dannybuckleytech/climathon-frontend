import React, { useState } from "react"
import {
  Flex,
  Text,
  Map,
  GoogleMapApiWrapper,
  Spinner
} from "@engaging-tech/components"
import GoogleMapReact from "google-map-react"

const cars = [
  { longitude: -0.020407, latitude: 51.529249 },
  { longitude: -0.020407, latitude: 51.529249 },
  { longitude: -0.020407, latitude: 51.529249 },
  { longitude: -0.020407, latitude: 51.529249 },
  { longitude: -0.020407, latitude: 51.529249 },
  { longitude: -0.020407, latitude: 51.529249 },
  { longitude: -0.020407, latitude: 51.529249 }
]

const mapCarCoordinates = () => {
  return cars.map(car => ({ lat: car.latitude, lng: car.longitude }))
}

const HeatMap = ({ updateMapRef }) => {
  const center = {
    lat: 51.529249,
    lng: -0.020407
  }

  const zoom = 11

  return (
    <Flex width={1/1} height={1000}>
      <GoogleMapReact
        ref={el => updateMapRef(el)}
        bootstrapURLKeys={{ key: "AIzaSyDW_e6uwIf4hjoYBsxmor_tBGtjF6C3g6o" }}
        defaultCenter={center}
        defaultZoom={zoom}
        heatmapLibrary
        heatmap={{
          positions: mapCarCoordinates(),
          options: { radius: 20, opacity: 0.6 }
        }}
        // onClick={this.onMapClick.bind(this)}
      />
    </Flex>
  )
}

export default HeatMap
