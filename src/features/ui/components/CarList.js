import React from "react"
import { gql, useSubscription } from "@apollo/client"
import { Flex, Text } from "@engaging-tech/components"

const testCars = [
  { longitude: -0.020407, latitude: 51.529249 },
  { longitude: -0.020407, latitude: 51.529249 },
  { longitude: -0.020407, latitude: 51.529249 },
  { longitude: -0.020407, latitude: 51.529249 },
  { longitude: -0.020407, latitude: 51.529249 },
  { longitude: -0.020407, latitude: 51.529249 },
  { longitude: -0.020407, latitude: 51.529249 }
]

const STREAM_CARS = gql`
  subscription {
    streamCarData {
      uuid
      latitude
      longitude
      emissionData {
        pm1
        pm2
        pm10
      }
    }
  }
`
const mapCarCoordinates = (cars, googleInstance) => {
  googleInstance.heatmap.data.clear()
  cars.forEach(car => (googleInstance.heatmap.data.push(new google.maps.LatLng(car.latitude, car.longitude))))
}

const CarList = ({mapRef}) => {
  const { loading, error, data } = useSubscription(STREAM_CARS)

  if (loading) return "Loading..."
  if (error) return `Error! ${error.message}`
  console.log(mapRef)
  if(mapRef){
    mapCarCoordinates(data.streamCarData, mapRef)
    // mapRef.heatmap.data = {
    //   positions: mapCarCoordinates(data.streamCarData, mapRef),
    //   options: { radius: 20, opacity: 0.6 }
    }


  return (
    <>
      <Flex flexDirection="column">
        <Text fontWeight={700} fontSize={4}>
          Real Time Cars
        </Text>
        {data.streamCarData.map(car => (
          <Flex
            key={car.uuid}
            flexDirection="column"
            bg="grey"
            width={200}
            p={2}
            m={2}
          >
            <Text fontWeight={700}>Coordinates:</Text>
            <Text>Longitude: {car.longitude}</Text>
            <Text>latitude: {car.latitude}</Text>
            <Text fontWeight={700} mt={2}>
              Emission Data:
            </Text>
            <Text>PM1.0: {car.emissionData.pm1}</Text>
            <Text>PM2.5: {car.emissionData.pm2}</Text>
            <Text>PM10: {car.emissionData.pm10}</Text>
          </Flex>
        ))}
      </Flex>
    </>
  )
}

export default CarList
