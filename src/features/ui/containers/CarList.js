import { useEffect } from "react"
import { connect } from "react-redux"

import CarList from "../components/CarList"

import {getMapRef} from "../store/ui.selectors"

const mapState = state => ({
  mapRef: getMapRef(state)
})
const CarListContainer = connect(mapState)(CarList)

export default CarListContainer
