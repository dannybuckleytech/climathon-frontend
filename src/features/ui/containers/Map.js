
import { connect } from "react-redux"

import Map from "../components/Map"

import {getMapRef} from "../store/ui.selectors"
import {updateMapRef} from "../store/ui.actions"

// const mapState = state => ({
//   mapRef: getMapRef(state)
// })
const mapDispatch = dispatch => ({
  updateMapRef: ref => dispatch(updateMapRef(ref))
})

const CarListContainer = connect(null, mapDispatch)(Map)

export default CarListContainer
