export const types = {
  UPDATE_MAP_REF: "[UI] UPDATE MAP REF",
}

export const updateMapRef = ref => ({
  type: types.UPDATE_MAP_REF,
  payload: ref
})
