import { types } from "./ui.actions"

export const initialState = {
  mapRef: null
}

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.UPDATE_MAP_REF:
      return { ...state, mapRef: action.payload }
    default:
      return state
  }
}
