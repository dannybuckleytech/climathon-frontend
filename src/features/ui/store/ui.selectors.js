import { createSelector } from "reselect"

const uiState = state => state.ui

export const getMapRef = createSelector(uiState, state => state.mapRef)
