const endpoints = {
  graphql: "/graphql"
}

const appPaths = {
}

export const devConfig = {
  isProd: false,
  api: {
    url: "https://dev-api.engaging.works",
    endpoints
  },
  oldApi: {
    url: "https://ew-dev.engaging.tech/v1/api",
    endpoints
  },
  engagingWorks: "https://new.ew-dev.engaging.tech",
  app: {
    url: "https://app.eb-dev.engaging.tech",
    paths: {
      ...appPaths
    }
  },
  keys: {
    stripe: "pk_test_VeSsgtPE5dAv66q6kcT3Hrht",
    ga: "test-xxxxx",
    gtmId: "GTM-TEST",
    facebookPixel: "123456",
    linkedin: "1234456"
  }
}

export const prodConfig = {
  isProd: true,
  api: {
    url: "https://api.engaging.works",
    endpoints
  },
  oldApi: {
    url: "https://engaging.works/v1/api",
    endpoints
  },
  engagingWorks: "https://engaging.works",
  app: {
    url: "https://app.engaging.business",
    paths: {
      ...appPaths
    }
  },
  keys: {
    stripe: "pk_live_dWGYPDgAXISlO23ao0jEZzcx",
    ga: "UA-127881707-4",
    gtmId: "GTM-WLZ5BMC",
    facebookPixel: "484970512262821",
    linkedin: "1369313"
  }
}
