import React from "react"
import express from "express"
import { Provider as StoreProvider } from "react-redux"
import { StaticRouter } from "@engaging-tech/routing"
import { renderToString } from "react-dom/server"
import {
  ServerStyleSheet,
  StyleSheetManager,
  ThemeProvider
} from "styled-components"
import {
  createConfig,
  initialiseServerConfig,
  initialiseClientConfig
} from "@engaging-tech/ssr-config"
import { Themes } from "@engaging-tech/components"

import App from "./App"
import store from "./store"
import { devConfig, prodConfig } from "./config"

createConfig({ dev: devConfig, prod: prodConfig })
initialiseServerConfig()

/* eslint-disable-next-line import/no-dynamic-require */
const assets = require(process.env.RAZZLE_ASSETS_MANIFEST)

const server = express()
server
  .disable("x-powered-by")
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR))
  .get("/*", (req, res) => {
    const sheet = new ServerStyleSheet()
    const context = {}

    const markup = renderToString(
      <StoreProvider store={store}>
        <StaticRouter context={context} location={req.url}>
          <StyleSheetManager sheet={sheet.instance}>
            <ThemeProvider theme={Themes.engagingBusiness}>
              <App />
            </ThemeProvider>
          </StyleSheetManager>
        </StaticRouter>
      </StoreProvider>
    )

    const styleTags = sheet.getStyleTags()
    sheet.seal()

    if (context.url) {
      res.redirect(context.url)
    } else {
      res.status(200).send(
        `<!doctype html>
    <html lang="">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta charset="utf-8" />
        <title>Engaging Business</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        ${initialiseClientConfig()}
        ${
          assets.client.css
            ? `<link rel="stylesheet" href="${assets.client.css}">`
            : ""
        }
        ${
          process.env.NODE_ENV === "production"
            ? `<script src="${assets.client.js}" defer></script>`
            : `<script src="${assets.client.js}" defer crossorigin></script>`
        }
        ${styleTags}
    </head>
    <body>
        <div id="root">${markup}</div>
    </body>
</html>`
      )
    }
  })

export default server
