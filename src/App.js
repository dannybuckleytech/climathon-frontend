import React from "react"
import { AllRoutes } from "@engaging-tech/routing"
import { ApolloProvider, ApolloClient, InMemoryCache } from "@apollo/client"
import { WebSocketLink } from "@apollo/client/link/ws"
import routes from "./routes"

const wsLink = process.browser
  ? new WebSocketLink({
      // if you instantiate in the server, the error will be thrown
      uri: `ws://localhost:7002/graphql`,
      options: {
        reconnect: true
      }
    })
  : null

const client = new ApolloClient({
  link: wsLink,
  cache: new InMemoryCache()
})

const App = () => (
  <>
    <ApolloProvider client={client}>
      <AllRoutes routes={routes} />
    </ApolloProvider>
  </>
)

export default App
