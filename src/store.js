import { createStore, combineReducers, applyMiddleware, compose } from "redux"
import { spawn } from "redux-saga/effects"
import createSagaMiddleware from "redux-saga"


import { reducer as uiReducer } from "./features/ui/store/ui.reducer"
import { saga as uiSaga } from "./features/ui/store/ui.sagas"

const sagaMiddleware = createSagaMiddleware()

const withDevTools =
  (typeof window !== "undefined" &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose

const rootReducer = combineReducers({
  ui: uiReducer
})

const store =
  typeof window !== "undefined"
    ? createStore(rootReducer, withDevTools(applyMiddleware(sagaMiddleware)))
    : createStore(rootReducer, withDevTools(applyMiddleware(sagaMiddleware)))

function* rootSaga() {
  yield spawn(uiSaga)
}

sagaMiddleware.run(rootSaga)

export default store
