import React from "react"
import { Provider as StoreProvider } from "react-redux"
import { hydrate } from "react-dom"
import { ThemeProvider, createGlobalStyle } from "styled-components"
import { ClientRouter } from "@engaging-tech/routing"
import { Themes } from "@engaging-tech/components"

import App from "./App"
import store from "./store"

/* eslint-disable import/no-dynamic-require */
const woffIcons = require(`${process.env.RAZZLE_PUBLIC_DIR}/fonts/EngagingTechIcons.woff`)
const ttfIcons = require(`${process.env.RAZZLE_PUBLIC_DIR}/fonts/EngagingTechIcons.ttf`)
/* eslint-enable import/no-dynamic-require */

const GlobalStyles = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500|Roboto+Slab:300,400,700');

  @font-face {
    font-family: "EngagingTechIcons";
    font-style: normal;
    font-weight: 400;
    src: 
      url(${woffIcons}) format("woff"),
      url(${ttfIcons}) format("truetype");
  }

  * {
    box-sizing: border-box;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  body {
    margin: 0;
    padding: 0;
  }

  a {
    color: inherit;
    text-decoration: inherit;
  }
`

hydrate(
  <StoreProvider store={store}>
    <ClientRouter>
      <ThemeProvider theme={Themes.engagingBusiness}>
        <>
          <GlobalStyles />
          <App />
        </>
      </ThemeProvider>
    </ClientRouter>
  </StoreProvider>,
  document.getElementById("root")
)

if (module.hot) {
  module.hot.accept()
}
