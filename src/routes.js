import React from "react"
import { ExternalLink } from "@engaging-tech/routing"
import { getConfig } from "@engaging-tech/ssr-config"

import Pages from "./pages"

const routes = [
  {
    path: "/",
    description: "Home",
    component: Pages.Home
  }
]

export default routes
