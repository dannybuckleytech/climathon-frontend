import React from "react"
import { Box } from "@engaging-tech/components"

import Nav from "../features/ui/components/Nav"

const PageLayout = ({ children }) => (
  <>
    <Nav />
    <Box as="main">{children}</Box>
  </>
)

export default PageLayout
