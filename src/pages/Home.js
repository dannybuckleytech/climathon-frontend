import React from "react"
import CarList from "../features/ui/containers/CarList"
import Map from "../features/ui/containers/Map"
import {
  Flex,
} from "@engaging-tech/components"

import PageLayout from "../layouts/PageLayout"

const Home = () => {

  return (
    <PageLayout>
    <Flex>
    <CarList />
      <Map />
    </Flex>

    </PageLayout>
  )
}

export default Home
